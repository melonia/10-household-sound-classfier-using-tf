# 10 sound classifier using tensorflow #



### Description of Files ###

+ a5_ckpt/ : folder of checkpoint 
+ a5_dnn.py : trainning file
+ feature_extraction2.py : wav file feature extraction file
+ file_info.sh : wav files info descripter. if U wanna run this, U need 'SoX'. 
+ flask_dnn.py : running DNN as web API using Flask
+ mktotaldata.py : make a extracted feature, each 10 folder's wav file 
+ mytotaldata.npz : Dataset npz file. 
+ num_of_data.py : count dataset for each class


jupyter notebook file for testing 
------
* plot_trim.ipynb
* Untitled1.ipynb
* Untitled2.ipynb
* Untitled3.ipynb
* Untitled4.ipynb
* Untitled5.ipynb
* Untitled.ipynb
* Kfold.ipynb
* librosa test.ipynb